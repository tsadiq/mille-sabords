import _ from "lodash";
import {Dispatch, SetStateAction} from "react";
import {
    ANIMALS,
    BOAT2,
    BOAT3,
    BOAT4,
    COINS,
    DIAMOND,
    DICE_BLADES,
    DICE_COIN,
    DICE_DIAMOND,
    DICE_MONKEY,
    DICE_PARROT,
    DICE_SKULL,
    GUARDIAN,
    PIRATE,
    SKULL1, SKULL2,
    TREASURE
} from "./const";

export interface Player {
    id: number;
    name: string;
    score: number;
}

export const defaultPlayers: Player[] = [
    {id: 1, name:"Player 1", score: 0},
    {id: 2, name:"Player 2", score: 0},
];

interface CardDef {
    [x: string]: number;
}

export type CardType = string & ("animals" | "treasure" | "diamond" | "coins" | "guardian" | "pirate" | "boat-2" | "boat-3" | "boat-4" | "skull-1" | "skull-2");

export const cards: CardDef = {
    [ANIMALS]: 4,
    [TREASURE]: 4,
    [DIAMOND]: 4,
    [COINS]: 4,
    [GUARDIAN]: 4,
    [PIRATE]: 4,
    [BOAT2]: 2,
    [BOAT3]: 2,
    [BOAT4]: 2,
    [SKULL1]: 3,
    [SKULL2]: 2,
};

export const cardTypes = _.keys(cards) as CardType[];

export type FaceType = string & ("blades" | "monkey" | "parrot" | "diamond" | "coin" | "skull");

export const DiceFaces = {
    [DICE_BLADES]: '⚔',
    [DICE_MONKEY]: '🐒',
    [DICE_PARROT]: '🦜',
    [DICE_DIAMOND]: '💎',
    [DICE_COIN]: '💰',
    [DICE_SKULL]: '☠',
};

export const faceList = _.keys(DiceFaces) as FaceType[];

export interface UseStateObject<T> {
    data: T,
    setter: Dispatch<SetStateAction<T>>
}

export interface GameContext {
    players: UseStateObject<Player[]>;
    currentPlayer: UseStateObject<number>;
    deck: UseStateObject<string[]>;
    discarded: UseStateObject<string>;
    hasDrawn: UseStateObject<boolean>;
    hasRolled: UseStateObject<number>;
    rolling: UseStateObject<boolean>;
    shuffle: UseStateObject<boolean>;
    clear: UseStateObject<boolean>;
    skullCount: UseStateObject<number>;
    skullIsland: UseStateObject<boolean>;
    dices: {
        [key: string]: UseStateObject<FaceType>;
        dice1: UseStateObject<FaceType>;
        dice2: UseStateObject<FaceType>;
        dice3: UseStateObject<FaceType>;
        dice4: UseStateObject<FaceType>;
        dice5: UseStateObject<FaceType>;
        dice6: UseStateObject<FaceType>;
        dice7: UseStateObject<FaceType>;
        dice8: UseStateObject<FaceType>;
    };
}

export const defaultContext = {
    players: {data: defaultPlayers, setter: (value: Player[]) => {}},
    currentPlayer: {data: 1, setter: (value: number) => {}},
    deck: {data: ['none'], setter: (value: string[]) => {}},
    discarded: {data: 'none', setter: (value: string) => {}},
    hasDrawn: {data: false, setter: (value: boolean) => {}},
    hasRolled: {data: 0, setter: (value: number) => {}},
    rolling: {data: false, setter: (value: boolean) => {}},
    shuffle: {data: true, setter: (value: boolean) => {}},
    clear: {data: false, setter: (value: boolean) => {}},
    skullCount: {data: 0, setter: (value: number) => {}},
    skullIsland: {data: false, setter: (value: boolean) => {}},
    dices: {
        dice1: {data: DICE_BLADES, setter: (() => {}) as Dispatch<SetStateAction<FaceType>>},
        dice2: {data: DICE_BLADES, setter: (() => {}) as Dispatch<SetStateAction<FaceType>>},
        dice3: {data: DICE_BLADES, setter: (() => {}) as Dispatch<SetStateAction<FaceType>>},
        dice4: {data: DICE_BLADES, setter: (() => {}) as Dispatch<SetStateAction<FaceType>>},
        dice5: {data: DICE_BLADES, setter: (() => {}) as Dispatch<SetStateAction<FaceType>>},
        dice6: {data: DICE_BLADES, setter: (() => {}) as Dispatch<SetStateAction<FaceType>>},
        dice8: {data: DICE_BLADES, setter: (() => {}) as Dispatch<SetStateAction<FaceType>>},
        dice7: {data: DICE_BLADES, setter: (() => {}) as Dispatch<SetStateAction<FaceType>>},
    },
}
