import React, {FunctionComponent, useContext} from 'react';
import {Game} from "./App";
import _ from 'lodash';

const Skulls: FunctionComponent = () => {
    const context = useContext(Game);
    const {data: skullCount} = context.skullCount;

    return <div className="skull-box">
        {_.times(skullCount, (n: number) => <span key={n}>☠</span>)}
        {_.times(3 - skullCount, (n: number) => <span key={n} className="transp">☠</span>)}
    </div>;
};

export default Skulls;
