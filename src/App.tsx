import React, {createContext, useEffect, useState} from 'react';
import './App.css';
import DiceTray from "./DiceTray";
import Deck from "./Deck";
import DiscardPile from "./DiscardPile";
import {defaultContext, defaultPlayers, GameContext} from "./settings";
import {DICE_BLADES} from "./const";
import Skulls from "./Skulls";
import Emoji from "./Emoji";
import PlayerList from "./PlayerList";

export const Game = createContext(defaultContext);

const App: React.FC = () => {
    const [players, setPlayers] = useState(defaultPlayers);
    const [currentPlayer, setCurrentPlayer] = useState(1);
    const [deck, setDeck] = useState(['none']);
    const [discarded, setDiscarded] = useState('none');
    const [hasDrawn, setDrawn] = useState(false);
    const [hasRolled, setRolled] = useState(0);
    const [skullCount, setSkullCount] = useState(0);
    const [skullIsland, setSkullIsland] = useState(false);
    const [rolling, setRolling] = useState(false);
    const [shuffle, setShuffle] = useState(true);
    const [clear, setClear] = useState(false);

    const [dice1, setDice1] = useState(DICE_BLADES);
    const [dice2, setDice2] = useState(DICE_BLADES);
    const [dice3, setDice3] = useState(DICE_BLADES);
    const [dice4, setDice4] = useState(DICE_BLADES);
    const [dice5, setDice5] = useState(DICE_BLADES);
    const [dice6, setDice6] = useState(DICE_BLADES);
    const [dice7, setDice7] = useState(DICE_BLADES);
    const [dice8, setDice8] = useState(DICE_BLADES);

    const context: GameContext = {
        players: {data: players, setter: setPlayers},
        currentPlayer: {data: currentPlayer, setter: setCurrentPlayer},
        deck: {data: deck, setter: setDeck},
        discarded: {data: discarded, setter: setDiscarded},
        hasDrawn: {data: hasDrawn, setter: setDrawn},
        hasRolled: {data: hasRolled, setter: setRolled},
        rolling: {data: rolling, setter: setRolling},
        shuffle: {data: shuffle, setter: setShuffle},
        clear: {data: clear, setter: setClear},
        skullCount: {data: skullCount, setter: setSkullCount},
        skullIsland: {data: skullIsland, setter: setSkullIsland},
        dices: {
            dice1: {data: dice1, setter: setDice1},
            dice2: {data: dice2, setter: setDice2},
            dice3: {data: dice3, setter: setDice3},
            dice4: {data: dice4, setter: setDice4},
            dice5: {data: dice5, setter: setDice5},
            dice6: {data: dice6, setter: setDice6},
            dice7: {data: dice7, setter: setDice7},
            dice8: {data: dice8, setter: setDice8},
        },
    };

    useEffect(() => {
        if (clear) {
            setSkullCount(0);
            setSkullIsland(false);
            setDrawn(false);
            setRolled(0);
            setClear(false);
        }
    }, [clear]);

    return (
        <Game.Provider value={context}>
            <div className="App">
                <PlayerList/>
                <div className="game floated">
                    <div className="cards-slots">
                        <Deck/>
                        <DiscardPile/>
                    </div>
                    <DiceTray/>
                    <Skulls/>
                </div>
                <div className="points-guide floated">
                    <div><Emoji emoji="💰"/>100</div>
                    <div><Emoji emoji="💎"/>100</div>
                    <div><Emoji emoji="3🎲"/>100</div>
                    <div><Emoji emoji="4🎲"/>200</div>
                    <div><Emoji emoji="5🎲"/>500</div>
                    <div><Emoji emoji="6🎲"/>1000</div>
                    <div><Emoji emoji="7🎲"/>2000</div>
                    <div><Emoji emoji="8🎲"/>4000</div>
                    <div><Emoji emoji="🎁"/>500</div>
                </div>
            </div>
        </Game.Provider>
    );
};

export default App;
