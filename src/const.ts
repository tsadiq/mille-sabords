import {CardType, FaceType} from "./settings";

export const DICE_BLADES: FaceType = 'blades';
export const DICE_MONKEY: FaceType = 'monkey';
export const DICE_PARROT: FaceType = 'parrot';
export const DICE_DIAMOND: FaceType = 'diamond';
export const DICE_COIN: FaceType = 'coin';
export const DICE_SKULL: FaceType = 'skull';

export const ANIMALS: CardType = "animals";
export const TREASURE: CardType = "treasure";
export const DIAMOND: CardType = "diamond";
export const COINS: CardType = "coins";
export const GUARDIAN: CardType = "guardian";
export const PIRATE: CardType = "pirate";
export const BOAT2: CardType = "boat-2";
export const BOAT3: CardType = "boat-3";
export const BOAT4: CardType = "boat-4";
export const SKULL1: CardType = "skull-1";
export const SKULL2: CardType = "skull-2";
