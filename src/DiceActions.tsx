import React, {useContext} from 'react';
import {Game} from "./App";
import {ANIMALS, COINS, DIAMOND, DICE_BLADES, DICE_COIN, DICE_DIAMOND, DICE_MONKEY, DICE_PARROT, DICE_SKULL, SKULL1, SKULL2} from "./const";

const DiceActions = () => {
    const context = useContext(Game);
    const {data: skullCount} = context.skullCount;
    const {data: hasDrawn} = context.hasDrawn;
    const {setter: setClear} = context.clear;
    const {data: rolling, setter: setRolling} = context.rolling;
    const {data: hasRolled, setter: setRolled} = context.hasRolled;
    const {data: discarded} = context.discarded;
    const {data: skullIsland} = context.skullIsland;
    const {data: players, setter: setPlayers} = context.players;
    const {data: currentPlayer, setter: setCurrentPlayer} = context.currentPlayer;

    const rollDices = () => {
        if (!rolling) {
            setRolled(hasRolled + 1);
            setRolling(true);

            setTimeout(() => {
                setRolling(false)
            }, 1000);
        }
    };

    const skullIslandPoints = () => {
        console.log('== Start skull island points calculation ================');

        let points = skullCount * -100;
        console.log(`${points} for ${skullCount} ☠ = ${points}`);

        if (discarded === 'pirate') {
            points = points * 2;
            console.log(`x2 points with Pirate card = ${points}`);
        }

        for (let player of players) {
            if (player.id !== currentPlayer) {
                player.score += points;
                setPlayers(players);
            }
        }

        setCurrentPlayer(currentPlayer + 1 > players.length ? 1 : currentPlayer + 1);
        setClear(true);
    };

    const finishTurn = () => {
        if (!hasRolled) {
            alert('You must roll the dices!');
            return;
        }

        if(skullIsland) {
            return skullIslandPoints();
        }

        let skulls = 0;
        if (discarded === SKULL1) {
            skulls = 1;
        } else if (discarded === SKULL2) {
            skulls = 2;
        }

        const faceCount = {
            [DICE_BLADES]: 0,
            [DICE_MONKEY]: 0,
            [DICE_PARROT]: 0,
            [DICE_DIAMOND]: discarded === DIAMOND ? 1 : 0,
            [DICE_COIN]: discarded === COINS ? 1 : 0,
            [DICE_SKULL]: skulls,
        };

        for (let dice of Object.keys(context.dices) as (keyof typeof context.dices)[]) {
            faceCount[context.dices[dice].data] += 1;
        }

        if (discarded === ANIMALS) {
            faceCount[DICE_MONKEY] += faceCount.parrot;
            faceCount.parrot = 0;
        }
        let points = 0;
        console.log('== Start points calculation ================');
        console.log(faceCount);
        for (let face of Object.keys(faceCount)) {
            if (face !== DICE_SKULL) {
                switch (faceCount[face as (keyof typeof faceCount)]) {
                    case 8:
                        points += 4000;
                        console.log(`4000 points for 8 x ${face} = ${points}`);
                        break;
                    case 7:
                        points += 2000;
                        console.log(`2000 points for 7 x ${face} = ${points}`);
                        break;
                    case 6:
                        points += 1000;
                        console.log(`1000 points for 6 x ${face} = ${points}`);
                        break;
                    case 5:
                        points += 500;
                        console.log(`500 points for 5 x ${face} = ${points}`);
                        break;
                    case 4:
                        points += 200;
                        console.log(`200 points for 4 x ${face} = ${points}`);
                        break;
                    case 3:
                        points += 100;
                        console.log(`100 points for 3 x ${face} = ${points}`);
                        break;
                }
            }
        }

        const currencies = faceCount.diamond * 100 + faceCount.coin * 100;
        points += currencies;
        console.log(`${currencies} points for ${faceCount.diamond}💎 and ${faceCount.coin}💰 = ${points}`);

        if (skullCount - skulls === 0) {
            points += 500;
            console.log(`500 points for all dices = ${points}`);
        }

        if (discarded === 'boat-2') {
            if (faceCount.blades >= 2) {
                points += 300;
                console.log(`300 points for 2⚔ boat = ${points}`);
            } else {
                points = -300;
                console.log(`-300 points for 2⚔ boat = ${points}`);
            }
        }

        if (discarded === 'boat-3') {
            if (faceCount.blades >= 3) {
                points += 500;
                console.log(`500 points for 3⚔ boat = ${points}`);
            } else {
                points = -500;
                console.log(`-500 points for 3⚔ boat = ${points}`);
            }
        }
        if (discarded === 'boat-4') {
            if (faceCount.blades >= 4) {
                points += 1000;
                console.log(`1000 points for 4⚔ boat = ${points}`);
            } else {
                points = -1000;
                console.log(`-1000 points for 4⚔ boat = ${points}`);
            }
        }

        if (discarded === 'pirate') {
            points = points * 2;
            console.log(`x2 points with Pirate card = ${points}`);
        }

        if (skullCount >= 3 && points > 0) {
            points = 0;
            console.log(`No points because of 3+ ☠ = ${points}`);
        }

        for (let player of players) {
            if (player.id === currentPlayer) {
                player.score += points;
                setPlayers(players);
            }
        }
        setCurrentPlayer(currentPlayer + 1 > players.length ? 1 : currentPlayer + 1);
        setClear(true);
    };

    const canRoll = skullCount < 3 || skullIsland;

    return (canRoll || rolling
            ? (hasDrawn
                    ? (
                        <React.Fragment>
                            <button onClick={finishTurn}>
                                {rolling ? '...' : 'Finish ✔'}
                            </button>
                            <button onClick={rollDices}>
                                {rolling ? '...' : 'Roll! 🎲'}
                            </button>
                        </React.Fragment>
                    )
                    : (<p>Draw a card</p>)
            )
            : (
                <button onClick={finishTurn}>
                    Next player !
                </button>
            )
    );
};

export default DiceActions;
