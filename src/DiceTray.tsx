import React, {FunctionComponent, useContext, useEffect} from 'react';
import Dice from "./Dice";
import {Game} from "./App";
import DiceActions from "./DiceActions";
import {DICE_SKULL, SKULL1, SKULL2} from "./const";

const DiceTray: FunctionComponent = () => {
    const context = useContext(Game);
    const {data: rolling} = context.rolling;
    const {data: hasRolled} = context.hasRolled;
    const {setter: setSkullCount} = context.skullCount;
    const {data: discarded} = context.discarded;
    const {setter: setSkullIsland} = context.skullIsland;

    useEffect(() => {
        if (!rolling) {
            let count = 0;

            if (discarded === SKULL1)
                count += 1;

            if (discarded === SKULL2)
                count += 2;

            for (let dice of Object.keys(context.dices) as (keyof typeof context.dices)[]) {
                if (context.dices[dice].data === DICE_SKULL) {
                    count++;
                }
            }
            setSkullCount(count);
            if (count >= 4 && hasRolled === 1) {
                setSkullIsland(true);
            }
        }
    }, [rolling]);

    return (
        <div className="dice-tray clearfix">
            <div>
                <Dice number={context.dices.dice1}/>
                <Dice number={context.dices.dice2}/>
                <Dice number={context.dices.dice3}/>
                <Dice number={context.dices.dice4}/>
            </div>
            <div className="clearfix">
                <Dice number={context.dices.dice5}/>
                <Dice number={context.dices.dice6}/>
                <Dice number={context.dices.dice7}/>
                <Dice number={context.dices.dice8}/>
            </div>
            <div className="clearfix"/>
            <DiceActions/>
        </div>
    );
};

export default DiceTray;
