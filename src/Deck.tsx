import React, {FunctionComponent, useContext, useEffect, useState} from 'react';
import {Game} from "./App";
import {cards, cardTypes} from "./settings";
import _ from 'lodash';
import {SKULL1} from "./const";

interface OwnProps {
}

type Props = OwnProps;

const Deck: FunctionComponent<Props> = (props) => {
    const [card, setCard] = useState(0);
    const context = useContext(Game);
    const {data: deck, setter: setDeck} = context.deck;
    const {data: shuffle, setter: setShuffle} = context.shuffle;
    const {setter: setDiscarded} = context.discarded;
    const {data: hasDrawn, setter: drawCard} = context.hasDrawn;

    const draw = () => {
        if (!hasDrawn) {
            drawCard(true);
            setCard(card + 1);
            setDiscarded(deck[card]);
        }
    };

    useEffect(() => {
        if (card > 0 && card <= deck.length && hasDrawn) {
            //we aren't at the end of the deck, so we can't re-shuffle now.
            setShuffle(false);
            return;
        }
        if (shuffle) {
            const deck: string[] = [];
            for (let type of cardTypes) {
                for (let i = 0; i < cards[type]; i++) {
                    deck.push(type);
                }
            }
            setDeck(_.shuffle(deck));
            setShuffle(false);
            setDiscarded('none');
            setCard(0);
        }
    }, [setDeck, shuffle, setShuffle]);

    return (
        <React.Fragment>
            <div className={`deck floated ${card >= deck.length ? 'empty' : ''}`} onClick={draw}>
                <span className="card-back">⚔</span>
            </div>
        </React.Fragment>
    );
};

export default Deck;
