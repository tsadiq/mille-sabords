import React, {FunctionComponent, useContext} from 'react';
import {Game} from "./App";

interface OwnProps {
}

type Props = OwnProps;

const PlayerList: FunctionComponent<Props> = (props) => {
    const context = useContext(Game);
    const {data: players} = context.players;
    const {data: currentPlayer} = context.currentPlayer;

    return (
        <div className="player-list floated">
            {players.map((player) => (
                <div key={player.id} className={currentPlayer === player.id ? 'current' : ''}>{`${player.name}: ${player.score}`}</div>
            ))}
        </div>
    );
};

export default PlayerList;
