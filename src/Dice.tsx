import React, {FunctionComponent, useContext, useEffect, useState} from 'react';
import {Game} from "./App";
import _ from 'lodash';
import {DiceFaces, faceList, FaceType, UseStateObject} from "./settings";
import {DICE_SKULL} from "./const";

interface OwnProps {
    number: UseStateObject<FaceType>;
}

type Props = OwnProps;

const Dice: FunctionComponent<Props> = ({number}) => {
    const [face, setFace] = useState(0);
    const [faces,] = useState(_.shuffle(faceList));
    const [skull, setSkull] = useState(false);
    const [locked, setLocked] = useState(false);
    const [started, setStarted] = useState(false);
    const context = useContext(Game);
    const {data: rolling} = context.rolling;
    const {data: skullCount} = context.skullCount;
    const {data: clear} = context.clear;
    const {data: dice, setter: setDice} = number;
    const {data: hasRolled} = context.hasRolled;

    useEffect(() => {
        if (locked || skull || !started) {
            return;
        }
        const roll = setInterval(() => {
            if (rolling) {
                const rand = parseInt((Math.random() * 5).toFixed(0));
                setFace(rand);
                setDice(faces[rand]);
            }
        }, 100);
        return () => {
            clearInterval(roll);
        }
    }, [rolling, started]);

    useEffect(() => {
        if (rolling) {
            setStarted(true);
        }
        if (!rolling && started && faces[face] === DICE_SKULL) {
            setSkull(true);
        }
    }, [rolling]);

    useEffect(() => {
        if (clear) {
            setLocked(false);
            setSkull(false);
        }
    }, [clear]);

    const lock = () => {
        if (hasRolled) setLocked(!locked)
    };

    const classes = `dice floated ${locked ? 'locked' : ''} ${skull ? 'skull' : ''} ${skullCount >= 3 ? 'lost' : ''}`;
    return (
        <div className={classes} onClick={lock}>
            {DiceFaces[dice as FaceType]}
        </div>
    );
};

export default Dice;
