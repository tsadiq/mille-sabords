import React, { FunctionComponent } from 'react';

interface OwnProps {
    emoji: string;
}

type Props = OwnProps;

const Emoji: FunctionComponent<Props> = (props) => {
  return (
      <span role="img" aria-label="emoji">{props.emoji}</span>
  );
};

export default Emoji;
