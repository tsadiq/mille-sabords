import React, {FunctionComponent, useContext, useEffect} from 'react';
import {Game} from './App';
import {ANIMALS, BOAT2, BOAT3, BOAT4, COINS, DIAMOND, GUARDIAN, PIRATE, SKULL1, SKULL2, TREASURE} from "./const";

interface OwnProps {
}

type Props = OwnProps;

const DiscardPile: FunctionComponent<Props> = (props) => {
    const context = useContext(Game);
    const {data: discarded} = context.discarded;
    const {setter: setShuffle} = context.shuffle;
    const {setter: setSkullCount } = context.skullCount;

    const cardFace = () => {
        switch (discarded) {
            case PIRATE:
                return '🏴‍☠️';
            case TREASURE:
                return '🔒‍';
            case GUARDIAN:
                return '🙅‍♀️';
            case ANIMALS:
                return '🐵';
            case DIAMOND:
                return '💎';
            case COINS:
                return '💰';
            case BOAT2:
                return (<div>
                    <div className="swords">⚔⚔</div>
                    <div className="boat"><span role="img" aria-label="sailboat">⛵</span></div>
                    <div>300</div>
                </div>);
            case BOAT3:
                return (<div>
                    <div className="swords">⚔⚔⚔</div>
                    <div className="boat"><span role="img" aria-label="sailboat">⛵</span></div>
                    <div>500</div>
                </div>);
            case BOAT4:
                return (<div>
                    <div className="swords">⚔⚔⚔⚔</div>
                    <div className="boat"><span role="img" aria-label="sailboat">⛵</span></div>
                    <div>1000</div>
                </div>);
            case SKULL1:
                return '☠';
            case SKULL2:
                return (<div className="twoSkulls">☠<br/>☠</div>);
            default:
                return discarded;
        }
    };

    useEffect(() => {
        if (discarded === SKULL1) {
            setSkullCount(1);
        }
        if (discarded === SKULL2) {
            setSkullCount(2);
        }
    }, [discarded]);

    return (
        <div className={`discard-pile floated ${discarded === 'none' ? 'empty' : ''}`}>
            <div className={`card-face ${discarded}`} onClick={() => setShuffle(true)}>{cardFace()}</div>
        </div>
    );
};

export default DiscardPile;
